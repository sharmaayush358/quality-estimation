#-*- coding:utf8 -*-
'''
Created on Jun 16, 2017

@author: czm
'''
from RNN.rnn import train
from RNN.rnn_stack_pro import train

if __name__ == '__main__':
    
    valid_errors =train(
                    batch_size=60,
                    valid_batch_size=60,
                    dim=200, 
                    dim_word=500,
                    
                    dispFreq=1,
                    saveFreq=1000,
                    validFreq=100,
                    
                    saveto='RNN_model/RNN.en-de.npz',#RNN stack
                    final_model='RNN_model/RNN.en-de.npz',#RNN zhushi
                    datasets=['test/train.combine.en',
                              'test/train.combine.de'],
                    valid_datasets=['test/dev.combine.en',
                                    'test/dev.combine.de'],
                    dictionaries=['data/train.bpe.en.json',
                                  'data/train.bpe.de.json'],
                    hter=['test/train.hter',
                          'test/dev.hter'],
                    n_words_src=40000,
                    n_words_tgt=40000,
                    nmt_model='model_en_de/model.npz.best_bleu',
                    lrate=0.0001,  # learning rate
                    use_dropout=True,
                    patience=10,
                    optimizer='adadelta',
                    shuffle_each_epoch=True,
                    reload_=True,
                    overwrite=False,
                    sort_by_length=False,
                    maxlen=1000,
                    decay_c=0.,  # L2 regularization penalty
                    map_decay_c=0., # L2 regularization penalty towards original weights
                    clip_c=1.0
                )
    
    print valid_errors


import numpy
import os
import sys

VOCAB_SIZE_SRC = 18212
VOCAB_SIZE_TGT = 35754
SRCTAG = "en"
TRGTAG = "de"
DATA_DIR = "data"
TUNING_DIR = "tuning"
MODEL = "model_en_de"

from nematus.nmt import train


if __name__ == '__main__':
    validerr = train(saveto=MODEL+'/model.npz',
                    reload_=True,
                    dim_word=500,# 500 700
                    dim=1024,
                    n_words_tgt=VOCAB_SIZE_TGT,
                    n_words_src=VOCAB_SIZE_SRC,
                    decay_c=0.,
                    clip_c=1.,
                    lrate=0.0001,
                    optimizer='adadelta', #adam,adadelta
                    maxlen=50,
                    batch_size=4,
                    valid_batch_size=4,
                    datasets=[TUNING_DIR + '/train.bpe.' + SRCTAG, TUNING_DIR + '/train.bpe.' + TRGTAG],
                    valid_datasets=[TUNING_DIR + '/dev.bpe.' + SRCTAG, TUNING_DIR + '/dev.bpe.' + TRGTAG],
                    dictionaries=[DATA_DIR + '/train.bpe.' + SRCTAG + '.json',DATA_DIR + '/train.bpe.' + TRGTAG + '.json'],
                    validFreq=3000, #10000,3000
                    dispFreq=100,  #1000,100
                    saveFreq=10000, #30000,10000
                    #sampleFreq=10000,
                    sampleFreq=0,
                    use_dropout=True,
                    dropout_embedding=0.2, # dropout for input embeddings (0: no dropout)
                    dropout_hidden=0.2, # dropout for hidden layers (0: no dropout)
                    dropout_source=0.1, # dropout source words (0: no dropout)
                    dropout_target=0.1, # dropout target words (0: no dropout)
                    overwrite=False,
                    external_validation_script='./validate.sh')
    print validerr

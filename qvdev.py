from nematus.nmt import prepare_data,build_model,init_params
from nematus.theano_util import init_theano_params,load_params
from nematus.util import load_config
from nematus.data_iterator import TextIterator
import theano
import sys
import numpy
import cPickle as pkl
    
def get_qv(model='model_en_de/model.npz.best_bleu'):
    options = load_config(model)
    
    params = init_params(options)
    params = load_params(model, params)
    
    tparams = init_theano_params(params)
    
    trng,use_noise,x,x_mask,y,y_mask,\
        opt_ret, cost, ctx, tt = build_model(tparams,options)

    dev = TextIterator('test/test.2017.bpe.en', 'test/test.2017.bpe.de',
                            options['dictionaries'][0], options['dictionaries'][1],
                            n_words_source=options['n_words_src'], n_words_target=options['n_words_tgt'],
                            batch_size=options['batch_size'],
                            maxlen=1000,
                            sort_by_length=False)

    f_tt = theano.function([x,x_mask,y,y_mask],tt,name='f_tt')

    ################## dev #####################
    n_samples = 0
    with open('qv/test.2017.features','w') as fp:
        for x, y in dev:
            x, x_mask, y, y_mask = prepare_data(x, y, maxlen=1000,
			          n_words_src=options['n_words_src'],
			          n_words=options['n_words_tgt'])
            tt_ = f_tt(x,x_mask,y,y_mask)
            Wt = tparams['ff_logit_W'].get_value()
			        
            for j in range(y.shape[1]):
                for i in range(y.shape[0]):
                    if y_mask[i][j] == 1:
                        index = y[i][j]
                        qv = tt_[i,j,:].T*Wt[:,index]
                        qv = list(qv.flatten())

                        fp.write('\t'.join(map(lambda x:str(x), qv))+'\n')
                fp.write('\n')

            n_samples += y.shape[1]
            print 'processed:',n_samples,'samples ...'


    
if __name__ == '__main__':
    get_qv()






